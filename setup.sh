# source this file, don't execute it
source /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.11.1/tdaq/tdaq-06-01-01/installed/setup.sh 

# fix up LD_LIBRARY_PATH
_this=$(dirname $(readlink -f ${BASH_SOURCE[0]-$0}))
_new_ld=""
for dir in $(echo $LD_LIBRARY_PATH | tr ':' ' ')
do
   if ! echo $dir | grep -q external 
   then
      if [ -z "${_new_ld}" ]; then
         _new_ld=${dir}
      else
         _new_ld=${_new_ld}:${dir}
      fi
   fi
done


# now add the new externals areas
_new_ld=${_new_ld}:${_this}/tdaq/${CMTCONFIG}/lib:${_this}/dqm-common/${CMTCONFIG}/lib:${_this}/tdaq-common/${CMTCONFIG}/lib
export LD_LIBRARY_PATH=${_new_ld}

TDAQ_PYTHONPATH=$(echo ${TDAQ_PYTHONPATH}  | sed 's;LCG_81c;sw/lcg/releases/LCG_81c;g')
QTDIR=$(echo ${QTDIR} | sed 's;LCG_81c;sw/lcg/releases/LCG_81c;g')
ROOTSYS=$(echo ${ROOTSYS} | sed 's;LCG_81c;sw/lcg/releases/LCG_81c;g')
MANPATH=$(echo ${MANPATH} | sed 's;LCG_81c;sw/lcg/releases/LCG_81c;g')
TDAQ_PYTHON_HOME=$(echo ${TDAQ_PYTHON_HOME} | sed 's;LCG_81c;sw/lcg/releases/LCG_81c;g')
PYTHONPATH=$(echo ${PYTHONPATH} | sed 's;LCG_81c;sw/lcg/releases/LCG_81c;g')

LCG_INST_PATH=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.11.1/sw/lcg/releases

unset _this _new_ld 
